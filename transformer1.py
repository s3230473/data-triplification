#importing all libraries
import sys
import pandas as pd
from rdflib import Graph, Literal, RDF, Namespace, URIRef
import rdflib
from rdflib.namespace import XSD
import os

print(pd.__version__)
print(rdflib.__version__)

def create_rdf_graph(data, output_path):
    # Defining namespaces
    SAREF = Namespace("https://saref.etsi.org/core/")
    EX = Namespace("http://example.com/EnergyMeasurement/")

    # Initialize the graph and bind namespaces
    g = Graph()
    g.bind("saref", SAREF)
    g.bind("ex", EX)

    # RDF Classes and Properties
    EnergyMeasurement = SAREF.EnergyMeasurement
    hasEnergy = SAREF.hasEnergy
    hasTimestamp = SAREF.hasTimestamp

    # Process each column for the first valid or NaN value. 
    timestamp_col = 'utc_timestamp'
    for col in data.columns:
        if col != timestamp_col:
            first_valid_index = data[col].first_valid_index()
            if first_valid_index is not None:
                value = data.at[first_valid_index, col]
                utc_timestamp = data.at[first_valid_index, timestamp_col].strip()  # Correct timestamp for each value
                measurement_uri = EX[f"Measurement_{col}_{utc_timestamp.replace(' ', '_').replace(':', '-')}_"]
                g.add((measurement_uri, RDF.type, EnergyMeasurement))
                g.add((measurement_uri, hasTimestamp, Literal(utc_timestamp, datatype=XSD.dateTime)))
                if pd.isna(value):
                    g.add((measurement_uri, hasEnergy, Literal("NaN", datatype=XSD.string)))  # Handle NaN value, decimal values will be skipped. 
                else:
                    g.add((measurement_uri, hasEnergy, Literal(value, datatype=XSD.decimal)))

    g.serialize(destination=output_path, format='turtle')
    print(f"RDF graph has been serialized to {output_path}")

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: python transformer.py path/to/dataset.csv")
        sys.exit(1)

    input_path = sys.argv[1]
    if not os.path.exists(input_path):
        print("Error: File does not exist.")
        sys.exit(1)

    data = pd.read_csv(input_path)
    output_path = os.path.join(os.path.dirname(__file__), 'graph.ttl')
    create_rdf_graph(data, output_path)
